package net.minecraft.src;

public class TextureAtlasSprite implements Icon {
    public int getWidth() {
        return 0;
    }

    public int getHeight() {
        return 0;
    }

    public float getMinU() {
        return 0;
    }

    public float getMaxU() {
        return 0;
    }

    public float getInterpolatedU(double u) {
        return 0;
    }

    public float getMinV() {
        return 0;
    }

    public float getMaxV() {
        return 0;
    }

    public float getInterpolatedV(double v) {
        return 0;
    }

    public String getIconName() {
        return null;
    }

    public int getSheetWidth() {
        return 0;
    }

    public int getSheetHeight() {
        return 0;
    }

    public TextureAtlasSprite(String name) {
    }

    public void init(int tilesheetWidth, int tilesheetHeight, int x0, int y0, boolean flipped) {
    }

    public int getX0() {
        return 0;
    }

    public int getY0() {
        return 0;
    }

    public void updateAnimation() {
    }

    public void copy(TextureAtlasSprite stitched) {
    }
}